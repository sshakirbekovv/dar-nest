import { Post, Headers } from "@nestjs/common";
import { Body } from "@nestjs/common";
import { Get } from "@nestjs/common";
import { Controller } from "@nestjs/common"
import { LoginRequest, RegistrationRequest } from "src/shared/types";
import { AuthService } from "./auth.service";

@Controller('auth')

export class AuthController {
    
    constructor(private readonly authService: AuthService) {}

    @Post('login')
    postLogin(@Body() data: LoginRequest) {
        return this.authService.login(data);
    }

    @Get('profile')
    getProfile(@Headers('Authorization') token: string) {
        return this.authService.getProfile(token);
    }

    @Post('registration')
    postRegister(@Body() data:  RegistrationRequest) {
       return this.authService.register(data);
  }

}