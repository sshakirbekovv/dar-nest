import { BadRequestException, ForbiddenException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { LoginRequest, LoginResponse, RegistrationRequest } from 'src/shared/types';


const users = [
    {
        username: 'sshakirbekovv',
        password: '123',
        token: '12345678',
        firstName: 'Yerkebulan',
        lastName: 'Shakirbekov',
        avatar: 'https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg'
    }
];

let token = 1;
@Injectable()
export class AuthService {
    [x: string]: any;

    login(data: LoginRequest): LoginResponse {

        if(!data.password || !data.username) {
            throw new BadRequestException();
        }

        const user = users
            .find(u => u.username === data.username && u.password === data.password);
        if(user) {
            return {
                token: user.token
            }
        }
        throw new UnauthorizedException();
    }

    getProfile(token: string) {
        const user = users.find(u => u.token === token);

        if (user) {
            return {
                ...user,
                password: ''
            }
        }
        throw new NotFoundException();
    }

    register(data: RegistrationRequest): LoginResponse {
        
        if (!data.password || !data.username) {
            throw new BadRequestException();
        }
        const user = 
              users.find((u) => u.username === data.username);

        if (user) {
            throw new BadRequestException('You are already authorized!');
        }
    
        let current = token.toString();
        token += 1;
    
        users.push(
            { 
                username: data.username,
                password: data.password,
                token: current,
                firstName: data.firstname,
                lastName: data.lastname,
                avatar: data.avatar
            }
        );
    
        return {
            token: current
        }

    }

}
