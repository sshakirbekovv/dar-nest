import { Controller, Get, Param, Res } from '@nestjs/common';
import { Response } from 'express';
import { AppService } from './app.service';

@Controller('')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('test/:id/:name')
  getTest(
    @Param('id') id: string,
    @Param('name') name: string,
    @Res() res: Response,
    @Res() req): string {
    return `DARLAB - ${id} - ${name}`;
  }

  @Get('check')
  getCheck() {
    return {
      status: 'ok'
    }
  }

}
