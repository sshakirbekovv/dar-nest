export interface LoginRequest {
    username: string;
    password: string;
}

export interface LoginResponse {
    token: string;
}

export interface RegistrationRequest {
    firstname: string;
    lastname: string;
    username: string;
    password: string;
    avatar: string;
}
  